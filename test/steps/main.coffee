assert = require('assert')
English = require('yadda').localisation.English
request = require('request')
R = require 'ramda'

#Global variables used for simplictiry, better to implement "scope" object and clear on teardown
$response = null;

module.exports = do ->
  English.library()

  .given 'simple node.js server', (next) ->
    server = require '../../server'
    server.run next

  .then 'it listens on $URL', (url, next) ->
    request url, (error, response, body) ->
      if error
        throw new Error(error)
      next()
  .then '$URL response $RESPONSE', (url, testResponse, next) ->
    request url, (error, response, body) ->
      throw new Error error if error
      assert.equal body, testResponse
      next()

  .given 'open $URL', (url, next) ->
    request url, (error, response, body) ->
      throw new Error error if error
      throw new Error body if response.statusCode > 299
      $response = JSON.parse body
      next()

  .then 'response json should $containOrEqual $KEY: $VALUES', (containOrEqual, key, values, next) ->
    values = values.replace(/,\s+/g,',').split ','
    responseValues = R.pluck key, $response
    if containOrEqual is 'contain'
      assert.deepEqual((R.difference values, responseValues), [])
    else
      assert.deepEqual values, responseValues
    next()
  .then 'response json should be empty', (next) ->
    assert.deepEqual $response, []
    next()
