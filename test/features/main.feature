Feature: Test all features
Scenario: run server
  Given simple node.js server
  Then it listens on http://localhost:9000
  Then http://localhost:9000/test response github api

Scenario: Create a route to get a user's repositories
  Given open http://localhost:9000/terraqout/repositories/all
  Then response json should contain name: analitics, angular-bower-to-cdnjs, cmf, command-palette, config

Scenario: Create a route to search a user's repository
  Given open http://localhost:9000/terraqout/repositories/search/42746788
  Then response json should equal name: hydrogen

Scenario: False search scenario
  Given open http://localhost:9000/terraqout/repositories/search/12312312313123
  Then response json should be empty

Scenario: Create a route to filter a user's repositories by name
  Given open http://localhost:9000/terraqout/repositories/filter/name/^an
  Then response json should equal name: analitics, angular-bower-to-cdnjs

Scenario: False filter scenario
  Given open http://localhost:9000/terraqout/repositories/filter/id/mocha
  Then response json should be empty
