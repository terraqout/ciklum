# Here I din't use authorization to achive more requests per hour and ignore pagination
# for simplicity purposes
github = new (require 'github')(config.github)
cache = require('cache-manager').caching(config.cache)

#ramda is my favorite lodash/underscore/others alternative, because of:
#1. Automatic curring
#2. (functor, data) and not classic (data, functor) function declarations
# with this approach you can define your functor like I do with keyFilter and plainValuesMatcher
# and so better function composition
# you can achive this with partial application in any library but with extra code
R = require 'ramda'

plainValuesMatcher = (repo) ->
  RegExp(query).test R.values(repo).join(';')

keyFilter = (repo) ->
  RegExp(value).test repo[key]

# Here I could create a "class" to look like OOP-minded. But there is no state here to deal with.
# so plain structure or even couple of exported functions would be enough
# I prefer composition over inheritance so I wouldn't "extend" this service in a future
module.exports =
  # of course, I can use promisses/generators/deasyncify/monads/AOP here
  # but when you write functions that are small, easy to understand and name them properly
  # you'll never end with Pyramid Of Doom or any other "hipsteria" terms
  list: (username, cb) ->
    cache.wrap "repo/#{username}/all", (cacheCb) ->
      github.repos.getFromUser user: username, cacheCb
    , cb

  search: (username, query, cb) ->
    @list username, criticalError (results) ->
      cb null, (R.filter plainValuesMatcher, results)

  filter: (username, key, value, cb) ->
    @list username, criticalError (results) ->
      cb null, (R.filter keyFilter, results)
