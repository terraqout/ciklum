For backend focused developers:

Overview

Build an API that consumes the Github API to find repositories for a given user.

The purpose of the task is to highlight your abilities on the backend. Feel free to use whichever 

libraries and frameworks you feel comfortable with. We understand that skills are transferable. 

Note that we use Node, Hapi, MySQL,MongoDB, OrientDB and are pushing 

into ES6/7 features but can live with other technologies and older JS techniques. Just be ready 

to talk about your choices.

Requirements

 Create a route to get a user's repositories

 Create a route to search a user's repository

 Create a route to filter a user's repositories by name

Notes

 Consider the following things as ways you can show-off your skills:

 Caching

 Authentication

 RESTful API design

 We want to be able to start the application as easily as possible

 Feel free to show off with your code

 Be ready to talk about all aspects of your code - We're developers too, we just want to talk 

code
