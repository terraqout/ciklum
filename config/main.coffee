# Using plain coffee file as Dependency Injection config have some benefits:
# you can eval chunks of code in your config
# even traverse and modify it with function defined as part of config
# you can make some parts of config only readable with getters
# you can evel environment variables inline
# you can require parts of config or even use recursive file lookup for that
# you can merge basic config with environment specific ones
module.exports =
  isDevelopment: -> process.env.NODE_ENV != 'production'

  app:
    port: process.env.PORT or 9000
    hostname: process.env.HOSTNAME or '127.0.0.1'

  cache:
    # store engines can be set to external modules: node-cache-manager-redis, node-cache-manager-mongodb
    # or selfwritten store engine, but for simplicity purposes we use memory store
    store: process.env.CACHE_STORE or 'memory'
    max: 100
    ttl: 10000
  # Here we set service that works with repo api. Using global config
  # for all services give us ability to replace them with each other
  # only changing one line without changing "default configs", etc.
  # In a more complicated setup I would use a wrapper function
  # dealing with __dirname and other stuff
  repositoryFinder: './services/github'

  github:
    version: "3.0.0"
