# Generally I don't like highly commented code. If workflow can't be understood without them,
# this should be a bad code. Here I comment my concepts and not code itself.

# I'm using coffee-script because I think it easier to understand than es6/7 code.
# I'm familiar with es6 features but didn't find something really neat that isn't
# implemented in coffee. Except 'let', but here we have 'do variable' pattern that
# covers 'let' use cases and even more.
#
# Of course I know you prefer JS, this isn't a problem for me at all.

#exporting run function, so it can be used in index.js and in tests
module.exports =
  run: (cb) ->

    # http module can be used, but piping outputs and other lowlevel stuff
    # break readability and single responsibility principle
    # koa, Hapi, strongloop are great options too, but not for this example app
    express = require('express')
    app = express()

    # Exposing config as global variable may look like as bad practice, but:
    # 1. Using global service locator for that is redundunt in such small application
    # 2. And besides that service locator is global too, or it injects, or any other magic
    # so it's exposed but another way. Global variable isn't evil, but fanboys are
    # 3. requiring config at every place you need isn't portable, unpredictably for
    # refactoring and hard to maintain.
    global.config = require './config/main'

    #define service that will do all work
    repositoryFinder = require config.repositoryFinder

    #this is a wrapper function that I use for functions with standard inteface in node
    # (error, result) to avoid antipattern "throw Error(error) if error" in any method
    # Generally this function should be way more complicated with newrelic/ga/logstash
    # or any other(maybe selfhosted) log system for production
    # With aop/event system it can be converted to separate concern fixer of all errors
    # Another way to achive this behaviour: middleware
    global.criticalError = (cb) -> (error, result) ->
      if error
        console.error "ERROR:", error
        throw new Error error if config.isDevelopment()
      cb result

    #Routes should be moved to route file/files
    # and controller functions to controllers folder in separate files
    # but not if there are only three of them
    if config.isDevelopment()
      app.get '/test', (req, res) -> res.send 'github api'

    app.get '/:username/repositories/all', (req, res) ->
      repositoryFinder.list req.params.username, criticalError (r) ->
        res.jsonp r

    app.get '/:username/repositories/filter/:key/:value', (req, res) ->
      repositoryFinder.filter req.params.username, req.params.key, req.params.value, criticalError (r) ->
        res.jsonp r

    app.get '/:username/repositories/search/:query', (req, res) ->
      repositoryFinder.search req.params.username, req.params.query, criticalError (r) ->
        res.jsonp r

    #standard server run
    server = app.listen config.app.port, config.app.hostname, ->
      console.log 'Test app listens on http://%s:%s', config.app.hostname, config.app.port
      cb() if cb

  terminate: (cb) ->
    server.close cb
